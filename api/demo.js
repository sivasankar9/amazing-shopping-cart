const _ = require('lodash');

class DemoData {
  constructor(d, name) {
    let data = _.cloneDeep(d);
    if (!Array.isArray(data)) {
      data = [d];
    }
    this.data = data;
  }

  find(predicate = {}, a = this.data) {
    return _.filter(a, predicate);
  }

  findOne(predicate = {}, a = this.data) {
    const data = _.filter(a, predicate);
    if (data.length < 1) {
      return {};
    }
    return data[0];
  }
}

module.exports = DemoData;
