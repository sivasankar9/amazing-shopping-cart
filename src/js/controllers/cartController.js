'use strict';

angular.module('amazingShopingCart')
.controller('cartController', ['$scope', 'cartResolve', function($scope, cartResolve){
	$scope.welcome ='cart';
	$scope.datas = cartResolve.data;
}]);
