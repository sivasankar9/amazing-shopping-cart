'use strict';

angular.module('amazingShopingCart')
.factory('myFactory', ['$http',function($http) {
	function getData(){
      var url = 'product_items';
      return $http.get(url);
    }
  return{
  	getApiData:getData
  }
}]);
