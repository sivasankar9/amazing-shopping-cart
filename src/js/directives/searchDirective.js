angular.module('amazingShopingCart')
.directive("search",function(){
	return{
		restrict:"E",
		templateUrl:'./templates/searchTemplate.html',
		controller:'searchController'
	}
});
