angular.module('amazingShopingCart')
.directive("loader",function(){
  return{
    restrict:'E',
    template:'<span ng-class="active" ><i class="fa fa-spinner fa-spin" style="font-size:48px;color:red"></i></span>',
  }
});