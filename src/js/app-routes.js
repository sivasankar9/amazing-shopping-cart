'use strict';

angular.module('amazingShopingCart')
.config(['$routeProvider',function(routeProvider){
	routeProvider
	.when('/tryprime',{
		templateUrl:'templates/tryprimeTemplate.html',
		controller:'tryprimeController'
	})
	.when('/cart',{
		templateUrl: '/templates/cartTemplate.html',
		controller:'cartController',
		resolve:{
			cartResolve:['$q','myFactory',function($q,fct){
				var defer = $q.defer();
				fct.getApiData().then(function(data){
					defer.resolve(data.data);
				});
				return defer.promise;
			}]
		}
	});

}]);